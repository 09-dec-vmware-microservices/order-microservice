# write the instructions to build the image from the source code
# set up the base OS
# Install JDK
# build the jar file
# run the jar file

FROM openjdk:11 as base
WORKDIR /app

# copy from source to destination image
COPY .mvn .mvn
COPY mvnw .
COPY pom.xml .

# give the mvnw executable permissions on the container
# Spin up a container from the image and run the below command and then burn the container to an image again
RUN chmod +x ./mvnw
RUN ./mvnw -B dependency:go-offline

# compiling the source code
COPY src src
# The below command generates the artifact innside the target directory
RUN ./mvnw package -DskipTests
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

#shippable image
FROM openjdk:11.0.4-jre-slim-buster as stage

ARG DEPENDENCY=/app/target/dependency

# Copy the dependency application file from builder stage artifact
COPY --from=base ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=base ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=base ${DEPENDENCY}/BOOT-INF/classes /app

EXPOSE 8222
RUN apt-get update && apt-install -y curl
ENTRYPOINT ["java", "-cp", "app:app/lib/*", "com.classpath.ordermicroservice.OrderMicroserviceApplication"]

