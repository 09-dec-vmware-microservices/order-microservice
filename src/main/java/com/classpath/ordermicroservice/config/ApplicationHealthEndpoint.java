package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class DBHealthChek implements HealthIndicator {
    private final OrderRepository orderRepository;
    @Override
    public Health health() {
        long numberOfRecords = this.orderRepository.count();
        if (numberOfRecords > 0){
            return Health.up().withDetail("DB-service","DB is up").build();
        }
        return Health.down().withDetail("DB-service","DB is down").build();
    }
}
@Component
@RequiredArgsConstructor
class KafkaHealthChek implements HealthIndicator {
    @Override
    public Health health() {
            return Health.up().withDetail("Kafka-service","Kafka is up").build();
    }
}