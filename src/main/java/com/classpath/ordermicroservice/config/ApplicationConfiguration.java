package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.model.LineItem;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

import java.time.ZoneId;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
public class ApplicationConfiguration implements ApplicationListener<ApplicationReadyEvent> {

    private final OrderRepository orderRepository;
    private final Faker faker = new Faker();
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        //functional style equivalent of traditional for loop
        IntStream.range(1, 10).forEach(index -> {
            //Use the builder pattern which can be used since we added the @Builder annotation on Order entity.
            Order order = Order.builder().customerName(faker.name().fullName())
                                         .date(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                                         .price(faker.number().randomDouble(2, 45_000, 55_000))
                                         .customerEmail(faker.internet().emailAddress())
                                         .lineItems(new HashSet<>())
                                         .build();
            IntStream.range(1, 4).forEach(value ->  {
                LineItem lineItem = LineItem.builder()
                                                .qty(faker.number().numberBetween(2, 5))
                                                .name(faker.commerce().productName())
                                                .price(faker.number().randomDouble(2, 20000, 30000))
                                        .build();
                order.addLineItem(lineItem);
            });
            this.orderRepository.save(order);
        });
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}