package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;


@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;

    private final RestTemplate restTemplate;

    //@CircuitBreaker(name = "inventoryservice", fallbackMethod = "fallback")
    @Retry(name = "retryService", fallbackMethod = "fallback")
    public Order saveOrder(Order order){
      log.info("Saving the order in the table {}", order);
        final Order savedOrder = this.orderRepository.save(order);
        final ResponseEntity<Integer> responseEntity = this.restTemplate.postForEntity("http://inventory-service/api/inventory", null, Integer.class);
        log.info("Fetching the response from inventory service :: {}", responseEntity.getBody());
        return savedOrder;
    }

    private Order fallback(Exception exception){
        log.error("Exception while updating the inventory :: ", exception.getMessage());
        return Order.builder().orderId(1111L).customerName("test").price(22000).customerEmail("test@gmail.com").build();
    }

    public Set<Order> fetchAllOrders(){
        return new HashSet<>(this.orderRepository.findAll());
    }
    public Order fetchOrderById(long orderId){
        return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("Invalid order id"));
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}