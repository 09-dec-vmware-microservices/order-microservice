package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.Set;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping
    public Set<Order> fetchOrders(){
        return this.orderService.fetchAllOrders();
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Order saveOrder(@RequestBody Order order){
        //order.getLineItems().forEach(lineItem -> lineItem.setOrder(order));
        return this.orderService.saveOrder(order);
    }

    @GetMapping("/{orderId}")
    public Order findOrderById(@PathVariable long orderId){
        return  this.orderService.fetchOrderById(orderId);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long orderId){
        this.orderService.deleteOrderById(orderId);
    }
}