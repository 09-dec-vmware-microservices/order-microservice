package com.classpath.ordermicroservice.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;

public class OrderTests {

    @Test
    public void testConstructor(){
        Order order = Order.builder().date(LocalDate.now()).price(34000).build();
        Assertions.assertNotNull(order);
    }
}